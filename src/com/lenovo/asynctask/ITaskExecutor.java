/**
 * Project:      AsyncTask
 * FileName:     IManageTaskExecutor.java
 * @Description: TODO
 * @author:      ligh4
 * @version      V1.0 
 * Createdate:   2015年1月12日 下午2:51:57
 * Copyright:    Copyright(C) 2014-2015
 * Company       Lenovo LTD.
 * All rights Reserved, Designed By Lenovo CIC.
 */
package com.lenovo.asynctask;

/**
 * 类 IManageTaskExecutor 的实现描述：TODO 类实现描述
 * 
 * @author ligh4 2015年1月12日下午2:51:57
 */
public abstract class ITaskExecutor extends IBaseTaskExecutor {

    public abstract Object execute(ITaskReferenceInternal taskRef);

    public abstract Object continue_execute(ITaskReferenceInternal taskRef);
}
