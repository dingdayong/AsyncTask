/**
 * Project:      AsyncTask
 * FileName:     ITimerTaskExecutor.java
 * @Description: TODO
 * @author:      ligh4
 * @version      V1.0 
 * Createdate:   2015年1月16日 上午10:44:15
 * Copyright:    Copyright(C) 2014-2015
 * Company       Lenovo LTD.
 * All rights Reserved, Designed By Lenovo CIC.
 */
package com.lenovo.asynctask;

/**
 * 类 ITimerTaskExecutor 的实现描述：TODO 类实现描述
 * 
 * @author ligh4 2015年1月16日上午10:44:15
 */
public abstract class ITimerTaskExecutor extends IBaseTaskExecutor {

    public abstract int intervalsMills();

    public abstract boolean isFinished();

    public abstract Object execute(ITaskReferenceInternal taskRef);

}
