/**
 * Project:      AsyncTask
 * FileName:     ExceptionUtil.java
 * @Description: TODO
 * @author:      ligh4
 * @version      V1.0 
 * Createdate:   2015年3月12日 上午10:22:15
 * Copyright:    Copyright(C) 2014-2015
 * Company       Lenovo LTD.
 * All rights Reserved, Designed By Lenovo CIC.
 */
package com.lenovo.asynctask.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 类 ExceptionUtil 的实现描述：TODO 类实现描述
 * 
 * @author ligh4 2015年3月12日上午10:22:15
 */
public class ExceptionUtil {

    public static String toString(Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        pw.flush();

        return sw.toString();
    }
}
