/**
 * Project:      AsyncTask
 * FileName:     DateUtil.java
 * @Description: TODO
 * @author:      ligh4
 * @version      V1.0 
 * Createdate:   2015年3月12日 上午9:45:20
 * Copyright:    Copyright(C) 2014-2015
 * Company       Lenovo LTD.
 * All rights Reserved, Designed By Lenovo CIC.
 */
package com.lenovo.asynctask.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 类 DateUtil 的实现描述：TODO 类实现描述
 * 
 * @author ligh4 2015年3月12日上午9:45:20
 */
public class DateUtil {

    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String now() {
        return format.format(new Date());
    }

    public static String format(Date date) {
        return format.format(date);
    }

    public static String fromat(String format, Date date) {
        SimpleDateFormat f = new SimpleDateFormat(format);
        return f.format(date);
    }

    public static Date parse(String source) {
        try {
            return format.parse(source);
        } catch (Exception e) {
            LogHelper.exception(e);
        }

        return null;
    }
}
