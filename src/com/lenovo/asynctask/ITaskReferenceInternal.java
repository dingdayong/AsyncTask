/**
 * Project:      AsyncTask
 * FileName:     ITaskReferenceInternal.java
 * @Description: TODO
 * @author:      ligh4
 * @version      V1.0 
 * Createdate:   2015年1月21日 下午3:56:55
 * Copyright:    Copyright(C) 2014-2015
 * Company       Lenovo LTD.
 * All rights Reserved, Designed By Lenovo CIC.
 */
package com.lenovo.asynctask;

/**
 * 类 ITaskReferenceInternal 的实现描述：TODO 类实现描述
 * 
 * @author ligh4 2015年1月21日下午3:56:55
 */
public interface ITaskReferenceInternal extends ITaskReference {

    public void setFlag(String flag);

    public void setState(TaskState state, String msg);

    public void setProgress(int progress);

    public void setProperty(String key, String value);

    public void setResult(Object result);

}
